package com.dharbor.talent.managervacations.countryservice.config;

import com.dharbor.talent.managervacations.countryservice.validator.Verificators;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jhonatan Soto
 */
@Configuration
public class VerifyConfig {
    @Bean
    public Verificators verificators() {
        return new Verificators();
    }
}
