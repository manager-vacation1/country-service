package com.dharbor.talent.managervacations.countryservice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Jhonatan Soto
 */

@Getter
@Setter
@Entity
@Table(name = ConstantsTableNames.CountryTable.Name)
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "handler"})
public class Country {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ConstantsTableNames.CountryTable.Id.NAME, unique = true)
    private Long id;

    @Column(
            name = ConstantsTableNames.CountryTable.Code.NAME,
            length = ConstantsTableNames.CountryTable.Code.LENGTH,
            nullable = false,
            unique = true
    )
    private String code;

    @Column(
            name = ConstantsTableNames.CountryTable.CountryName.NAME,
            unique = true
    )
    private String name;
}
