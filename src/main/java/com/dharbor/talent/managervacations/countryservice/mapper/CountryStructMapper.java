package com.dharbor.talent.managervacations.countryservice.mapper;

import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Mapper(componentModel = "spring")
public interface CountryStructMapper {
    CountryDTO countryToBookDto(Country country);

    List<CountryDTO> listCountryToCountriesDto(List<Country> countryList);
}
