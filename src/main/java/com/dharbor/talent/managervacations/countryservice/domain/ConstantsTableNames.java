package com.dharbor.talent.managervacations.countryservice.domain;
/**
 * @author Jhonatan Soto
 */
public final class ConstantsTableNames {
    static class CountryTable {
        static final String Name = "country_table";

        static class Id {
            static final String NAME = "country_id";
        }

        static class Code {
            static final String NAME = "country_code";
            static final int LENGTH = 3;
        }

        static class CountryName {
            static final String NAME = "country_name";
        }
    }
}

