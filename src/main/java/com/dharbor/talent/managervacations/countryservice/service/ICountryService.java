package com.dharbor.talent.managervacations.countryservice.service;

import com.dharbor.talent.managervacations.countryservice.domain.Country;

import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
public interface ICountryService {

    List<Country> getAllCountries();

    Optional<Country> optionalFindById(Long id);

    Country findByName(String name);

    Country save(Country country);

    void deleteById(Long id);

}
