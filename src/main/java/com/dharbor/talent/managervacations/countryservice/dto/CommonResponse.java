package com.dharbor.talent.managervacations.countryservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@AllArgsConstructor
public class CommonResponse {
    private String statusCode;
    private String message;
}
