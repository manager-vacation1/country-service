package com.dharbor.talent.managervacations.countryservice.usecase;

import com.dharbor.talent.managervacations.countryservice.common.Message;
import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import com.dharbor.talent.managervacations.countryservice.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.countryservice.mapper.CountryStructMapper;
import com.dharbor.talent.managervacations.countryservice.service.ICountryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
@Slf4j
public class GetCountryByNameUseCase {
    private ICountryService iCountryService;
    private CountryStructMapper countryStructMapper;
    private Message message;

    public CountryResponse execute(String name) {
        Country country = iCountryService.findByName(name);
        Optional<Country> optionalCountry =
                Optional.ofNullable(Optional.ofNullable(country)
                .orElseThrow(() -> new EntityNotFoundException(
                        message.getMessage("NoExist.Country.code.message"))));
        CountryDTO countryDTO = countryStructMapper.countryToBookDto(optionalCountry.get());
        return new CountryResponse(countryDTO);
    }
}
