package com.dharbor.talent.managervacations.countryservice.usecase;

import com.dharbor.talent.managervacations.countryservice.common.Message;
import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import com.dharbor.talent.managervacations.countryservice.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.countryservice.mapper.CountryStructMapper;
import com.dharbor.talent.managervacations.countryservice.service.ICountryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class GetCountryByIdUseCase {
    private ICountryService countryService;

    private CountryStructMapper countryStructMapper;

    private Message message;

    public CountryResponse execute(Long id) {
        Optional<Country> country = Optional.ofNullable(countryService.optionalFindById(id).
                orElseThrow(() -> new EntityNotFoundException(message.getMessage("NoExist.Country.code.message"))));
        CountryDTO countryDTO = countryStructMapper.countryToBookDto(country.get());
        return new CountryResponse(countryDTO);
    }
}
