package com.dharbor.talent.managervacations.countryservice.dto.response;

import com.dharbor.talent.managervacations.countryservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.countryservice.dto.CommonResponse;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class GetCountriesResponse extends CommonResponse {

    private List<CountryDTO> countryList;

    public GetCountriesResponse(List<CountryDTO> countryList) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.countryList = countryList;
    }
}
