package com.dharbor.talent.managervacations.countryservice.dto.response;

import com.dharbor.talent.managervacations.countryservice.constant.ResponseConstant;
import com.dharbor.talent.managervacations.countryservice.dto.CommonResponse;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class CountryResponse extends CommonResponse {

    private CountryDTO country;

    public CountryResponse(CountryDTO country) {
        super(ResponseConstant.StatusCodeResponse.SUCCESS_CODE, ResponseConstant.StatusCodeResponse.SUCCESS_MSG);
        this.country = country;
    }
}
