package com.dharbor.talent.managervacations.countryservice.controller;

import com.dharbor.talent.managervacations.countryservice.dto.request.CountryRequest;
import com.dharbor.talent.managervacations.countryservice.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.countryservice.dto.response.GetCountriesResponse;
import com.dharbor.talent.managervacations.countryservice.usecase.CreateCountryUseCase;
import com.dharbor.talent.managervacations.countryservice.usecase.GetCountriesUseCase;
import com.dharbor.talent.managervacations.countryservice.usecase.GetCountryByIdUseCase;
import com.dharbor.talent.managervacations.countryservice.usecase.GetCountryByNameUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Jhonatan Soto
 */

@RestController
@RequestMapping("/countries")
@AllArgsConstructor
@Tag(
        name = "Country",
        description = "Operation over countries"
)
public class CountryController {
    private CreateCountryUseCase createCountryUseCase;
    private GetCountriesUseCase getCountriesUseCase;
    private GetCountryByIdUseCase getCountryByIdUseCase;
    private GetCountryByNameUseCase getCountryByNameUseCase;

    @Operation(summary = "Create Counties")
    @PostMapping
    public ResponseEntity<CountryResponse> saveCountry(@RequestBody CountryRequest countryRequest) {
        CountryResponse countryResponse = createCountryUseCase.execute(countryRequest);
        return ResponseEntity.ok(countryResponse);
    }
    @Operation(summary = "Get all registered countries")
    @GetMapping
    public ResponseEntity<GetCountriesResponse> getCountries() {
        GetCountriesResponse countryResponse = getCountriesUseCase.execute();
        return ResponseEntity.ok(countryResponse);
    }
    @Operation(summary = "Get country by Id ")
    @GetMapping(value = "/{countryId}")
    public ResponseEntity<CountryResponse> getCountryById(@PathVariable Long countryId) {
        return ResponseEntity.ok(getCountryByIdUseCase.execute(countryId));
    }
    @Operation(summary = "Get country by Name ")
    @GetMapping("/countryname/{countryName}")
    public ResponseEntity<CountryResponse> getCountryByName(@PathVariable String countryName) {
        return ResponseEntity.ok(getCountryByNameUseCase.execute(countryName));
    }
}

