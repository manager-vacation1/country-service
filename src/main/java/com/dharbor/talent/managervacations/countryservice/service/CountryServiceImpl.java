package com.dharbor.talent.managervacations.countryservice.service;

import com.dharbor.talent.managervacations.countryservice.common.Message;
import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.repository.CountryRepository;
import com.dharbor.talent.managervacations.countryservice.validator.Verificators;
import com.dharbor.talent.managervacations.countryservice.validator.VerifyNullorEmptyCommand;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author Jhonatan Soto
 */
@Service
@AllArgsConstructor
public class CountryServiceImpl implements ICountryService {

    private CountryRepository countryRepository;

    private Message message;

    private Verificators verificators;


    @Override
    public List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    @Override
    public Optional<Country> optionalFindById(Long id) {
        return countryRepository.findById(id);
    }

    @Override
    public Country findByName(String name) {
        return countryRepository.findByName(name);
    }

    @Override
    public Country save(Country country) {
        verificators.verifyNullAndEmpty(new VerifyNullorEmptyCommand(country.getName(),
                message.getMessage("NotNull.country.name.message")));
        verificators.verifyNullAndEmpty(new VerifyNullorEmptyCommand(country.getCode(),
                message.getMessage("NotNull.country.code.message")));
        return countryRepository.save(country);
    }

    @Override
    public void deleteById(Long id) {
        countryRepository.deleteById(id);
    }


}
