package com.dharbor.talent.managervacations.countryservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
@NoArgsConstructor
public class CountryDTO {
    private Long id;
    private String code;
    private String name;
}
