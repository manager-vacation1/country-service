package com.dharbor.talent.managervacations.countryservice.exception;

/**
 * @author Jhonatan Soto
 */
public class DuplicateRegisterException extends RuntimeException{
    public DuplicateRegisterException(String message){
        super(message);
    }
}
