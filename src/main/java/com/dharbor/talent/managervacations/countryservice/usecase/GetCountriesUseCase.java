package com.dharbor.talent.managervacations.countryservice.usecase;

import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import com.dharbor.talent.managervacations.countryservice.dto.response.GetCountriesResponse;
import com.dharbor.talent.managervacations.countryservice.mapper.CountryStructMapper;
import com.dharbor.talent.managervacations.countryservice.service.ICountryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class GetCountriesUseCase {

   private ICountryService countryService;
   private CountryStructMapper countryStructMapper;

    public GetCountriesResponse execute() {
        List<CountryDTO> countryDTOList = countryStructMapper.listCountryToCountriesDto(countryService.getAllCountries());
        return new GetCountriesResponse(countryDTOList);

    }
}
