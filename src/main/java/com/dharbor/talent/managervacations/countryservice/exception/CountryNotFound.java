package com.dharbor.talent.managervacations.countryservice.exception;

/**
 * @author Jhonatan Soto
 */
public class CountryNotFound extends RuntimeException {
    public CountryNotFound(String message) {
        super(message);
    }
}
