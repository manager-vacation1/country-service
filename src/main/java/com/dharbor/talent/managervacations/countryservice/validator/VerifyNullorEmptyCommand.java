package com.dharbor.talent.managervacations.countryservice.validator;

import com.dharbor.talent.managervacations.countryservice.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Jhonatan Soto
 */
@Slf4j
public class VerifyNullorEmptyCommand implements Command {

    private final boolean validate;
    private final String message;

    public VerifyNullorEmptyCommand(String stringToVerify, String message) {
        this.validate = stringToVerify == null || stringToVerify.isEmpty();
        this.message = message;
    }

    @Override
    public void execute() {
        if (validate) {
            throw new BadRequestException(message);
        }
    }
}
