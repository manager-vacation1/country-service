package com.dharbor.talent.managervacations.countryservice.repository;

import com.dharbor.talent.managervacations.countryservice.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Jhonatan Soto
 */
public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findByName(String name);

}
