package com.dharbor.talent.managervacations.countryservice.dto.request;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Jhonatan Soto
 */
@Getter
@Setter
public class CountryRequest {
    private String code;
    private String name;
}
