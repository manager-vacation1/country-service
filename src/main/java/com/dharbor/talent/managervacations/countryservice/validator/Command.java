package com.dharbor.talent.managervacations.countryservice.validator;

/**
 * @author Jhonatan Soto
 */
public interface Command {
    void execute();
}
