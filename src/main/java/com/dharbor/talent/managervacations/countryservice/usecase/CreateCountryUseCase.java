package com.dharbor.talent.managervacations.countryservice.usecase;

import com.dharbor.talent.managervacations.countryservice.common.Message;
import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.dto.CountryDTO;
import com.dharbor.talent.managervacations.countryservice.dto.request.CountryRequest;
import com.dharbor.talent.managervacations.countryservice.dto.response.CountryResponse;
import com.dharbor.talent.managervacations.countryservice.exception.DuplicateRegisterException;
import com.dharbor.talent.managervacations.countryservice.mapper.CountryStructMapper;
import com.dharbor.talent.managervacations.countryservice.service.ICountryService;
import lombok.AllArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CreateCountryUseCase {

    private ICountryService countryService;

    private CountryStructMapper countryStructMapper;

    private Message message;

    public CountryResponse execute(CountryRequest countryRequest) {

        try {
            Country country = countryService.save(buildCountry(countryRequest));
            return buildCountryResponse(country);
        } catch (DataIntegrityViolationException ex) {
            throw new DuplicateRegisterException(
                    message.getMessage("Exists.Country.code.message"));
        }
    }

    private CountryResponse buildCountryResponse(Country country) {
        CountryDTO countryDTO = countryStructMapper.countryToBookDto(country);
        return new CountryResponse(countryDTO);
    }

    private Country buildCountry(CountryRequest countryRequest) {
        Country country = new Country();
        country.setCode(countryRequest.getCode());
        country.setName(countryRequest.getName());
        return country;
    }

}
