package com.dharbor.talent.managervacations.countryservice.dto.request;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CountryRequestTest {
    @Test
    void testConstructor() {
        CountryRequest actualCountryRequest = new CountryRequest();
        actualCountryRequest.setCode("Code");
        actualCountryRequest.setName("Name");
        assertEquals("Code", actualCountryRequest.getCode());
        assertEquals("Name", actualCountryRequest.getName());
    }
}

