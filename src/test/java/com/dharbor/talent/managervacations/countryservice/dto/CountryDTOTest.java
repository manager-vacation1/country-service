package com.dharbor.talent.managervacations.countryservice.dto;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class CountryDTOTest {
    @Test
    void testConstructor() {
        CountryDTO actualCountryDTO = new CountryDTO();
        actualCountryDTO.setCode("Code");
        actualCountryDTO.setId(123L);
        actualCountryDTO.setName("Name");
        assertEquals("Code", actualCountryDTO.getCode());
        assertEquals(123L, actualCountryDTO.getId().longValue());
        assertEquals("Name", actualCountryDTO.getName());
    }
}

