package com.dharbor.talent.managervacations.countryservice.controller;

import com.dharbor.talent.managervacations.countryservice.dto.request.CountryRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Rodrigo Aspeti
 */

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "spring.cloud.config.enabled=false")
@AutoConfigureTestDatabase
@ActiveProfiles("test")
class CountryControllerTest {

    @Autowired
    private TestRestTemplate client;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @Order(1)
    void testCreateCountry() {
        CountryRequest countryRequest = new CountryRequest();
        countryRequest.setCode("RA");
        countryRequest.setName("Ramses");

        ResponseEntity<String> response =
                client.postForEntity("/countries", countryRequest, String.class);
        System.out.println(response);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("{\"id\":1,\"code\":\"RA\",\"name\":\"Ramses\"}"));
    }

    @Test
    @Order(2)
    void testGetCountry(){
        ResponseEntity<String> response = client.getForEntity("/countries/1", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("{\"id\":1,\"code\":\"RA\",\"name\":\"Ramses\"}"));
    }

    @Test
    @Order(3)
    void testGetAllCountries(){
        ResponseEntity<String> response = client.getForEntity("/countries", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("countryList"));
    }

    @Test
    @Order(4)
    void testGetCountryByName() {
        ResponseEntity<String> response = client.getForEntity("/countries/countryname/Ramses", String.class);
        Assertions.assertEquals(200, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("{\"id\":1,\"code\":\"RA\",\"name\":\"Ramses\"}"));
    }

    @Test
    @Order(5)
    void testCountryNotFound() {
        ResponseEntity<String> response = client.getForEntity("/countries/countryname/egipto", String.class);
        System.out.println(response);
        Assertions.assertEquals(404, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("\"Country not Found\""));
    }

    @Test
    @Order(6)
    void testDuplicateCountry() {
        CountryRequest countryRequest = new CountryRequest();
        countryRequest.setCode("RA");
        countryRequest.setName("Ramses");

        ResponseEntity<String> response =
                client.postForEntity("/countries", countryRequest, String.class);
        Assertions.assertEquals(400, response.getStatusCodeValue());
        Assertions.assertTrue(response.getBody().contains("Country registration already exists"));
    }


}