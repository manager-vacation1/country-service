package com.dharbor.talent.managervacations.countryservice.repository;

import com.dharbor.talent.managervacations.countryservice.domain.Country;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class CountryRepositoryTest {

    @Autowired
    private CountryRepository undertest;

    @Test
    void findByNameNotNull() {
        Country country = new Country();
        country.setCode("AR");
        country.setName("Argentina");
        undertest.save(country);

        Country result = undertest.findByName(country.getName());
        assertThat(result).isNotNull();
    }

    @Test
    void findByName() {
        Country country = new Country();
        country.setCode("AR");
        country.setName("Argentina");
        undertest.save(country);

        Country result = undertest.findByName(country.getName());
        assertThat(result.equals(country)).isTrue();
    }
    @Test
    void findByNameNull() {
        Country result = undertest.findByName("Bolivia");
        assertThat(result).isNull();
    }
}