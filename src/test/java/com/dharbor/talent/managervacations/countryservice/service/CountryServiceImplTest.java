package com.dharbor.talent.managervacations.countryservice.service;

import com.dharbor.talent.managervacations.countryservice.common.Message;
import com.dharbor.talent.managervacations.countryservice.domain.Country;
import com.dharbor.talent.managervacations.countryservice.repository.CountryRepository;
import com.dharbor.talent.managervacations.countryservice.validator.Verificators;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class CountryServiceImplTest {

    @Mock
    private CountryRepository countryRepository;

    private Message message;

    private Verificators verificators;

    private CountryServiceImpl undertest;

    @BeforeEach
    void setUp(){
        verificators = new Verificators();
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        message = new Message(resourceBundleMessageSource);
        undertest = new CountryServiceImpl(countryRepository,message, verificators);
    }

    @Test
    void getAllCountries() {
        Country countryToSave = new Country();
        countryToSave.setName("Bolivia");
        countryToSave.setCode("BO");

        List<Country> countries = new ArrayList<>();
        countries.add(countryToSave);

        Mockito.when(countryRepository.findAll()).thenReturn(countries);
        List<Country> result = undertest.getAllCountries();

        verify(countryRepository).findAll();
        assertTrue(result.contains(countryToSave));
    }

    @Test
    void optionalFindById() {
        Country countryToSave = new Country();
        countryToSave.setName("Bolivia");
        countryToSave.setCode("BO");
        countryToSave.setId(1L);

        Mockito.when(countryRepository.findById(countryToSave.getId())).thenReturn(Optional.of(countryToSave));
        Optional<Country> result = undertest.optionalFindById(countryToSave.getId());

        verify(countryRepository).findById(countryToSave.getId());
        Assertions.assertEquals(true, result.isPresent());
    }

    @Test
    void findByName() {
        Country countryToSave = new Country();
        countryToSave.setName("Bolivia");
        countryToSave.setCode("BO");
        countryToSave.setId(1L);

        Mockito.when(countryRepository.findByName(countryToSave.getName())).thenReturn(countryToSave);
        Country result = undertest.findByName(countryToSave.getName());

        verify(countryRepository).findByName(countryToSave.getName());
        Assertions.assertTrue(result != null);
    }

    @Test
    void save() {
        Country countryToSave = new Country();
        countryToSave.setName("Bolivia");
        countryToSave.setCode("BO");

        Country countrySaved = new Country();
        countrySaved.setName(countrySaved.getName());
        countrySaved.setCode(countrySaved.getCode());
        countrySaved.setId(1L);

        Mockito.when(countryRepository.save(countryToSave)).thenReturn(countrySaved);
        Country result = undertest.save(countryToSave);

        verify(countryRepository, times(1)).save(countryToSave);
        assertTrue(result.getId().compareTo(1L) == 0);
    }

    @Test
    void deleteById() {
        Country countryToSave = new Country();
        countryToSave.setName("Bolivia");
        countryToSave.setCode("BO");
        countryToSave.setId(1L);

        Mockito.doNothing().when(countryRepository).deleteById(countryToSave.getId());
        verify(countryRepository,times(0)).deleteById(countryToSave.getId());

    }
}